FROM alpine:latest as system
WORKDIR /app
RUN apk add clang lld curl gcc musl-dev
RUN curl https://sh.rustup.rs -sSf -o rust.sh
RUN sh -f rust.sh -y

FROM system as dependencies

RUN ~/.cargo/bin/cargo init

COPY ./Cargo.* /app/

RUN mkdir .cargo
RUN ~/.cargo/bin/cargo vendor > .cargo/config
RUN ~/.cargo/bin/cargo install --path . --verbose

FROM dependencies as builder

COPY src /app/src

RUN ~/.cargo/bin/cargo build --release

FROM scratch as runtime
ARG PORT
ENV PORT=$PORT
COPY --from=builder /app/target/release/rest /

CMD ["/rest"]