use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder, middleware};
use log::info;
use std::env;

struct AppState {
    app_name: String,
}


#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello world! Hi")
}

#[post("/echo")]
async fn echo(req_body: String, data: web::Data<AppState>) -> impl Responder {
    info!("Post request; Server name: {}", data.app_name);
    HttpResponse::Ok().body(req_body)
}

async fn manual_hello() -> impl Responder {
    HttpResponse::Ok().body("Hey there!")
}

fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/tehnarenok")
            .route(web::get().to(|| HttpResponse::Ok().body("tehnarenok")))
            .route(web::head().to(|| HttpResponse::MethodNotAllowed())),
    );
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    simple_logger:: init_with_level(log::Level::Info).unwrap();

    HttpServer::new(|| {
        App::new()
            .wrap(middleware::NormalizePath::default())
            .wrap(middleware::Logger::default())
            .data(AppState {
                app_name: String::from("Tehnarenok Server")
            })
            .configure(config)
            .configure(rest::endpoints::test::test_scope)
            .service(hello)
            .service(echo)
            .route("/hey", web::get().to(manual_hello))
    })
    .bind(format!("0.0.0.0:{}", env::var("PORT").unwrap()))?
    .run()
    .await
}
