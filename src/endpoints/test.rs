use actix_web::{HttpResponse, get, post, web};
use log::info;
use serde::Deserialize;


#[derive(Deserialize)]
struct Info {
    username: String,
}

#[derive(Deserialize)]
struct DataSum {
    data: Vec<i32>
}

#[get("/")]
async fn hello_get() -> HttpResponse {
    info!("Endpoint from mod");
    HttpResponse::Ok().body("Hello world!")
}

#[post("/sum/")]
async fn sum_post(data: web::Json<DataSum>) -> HttpResponse {
    let mut sum: i32 = 0;
    for item in &data.data {
        sum += item;
    }
    if sum >= 0 {
        HttpResponse::Ok().body(format!("Sum is: {}", sum))
    } else {
        HttpResponse::Ok()
            .status(actix_web::http::StatusCode::UNPROCESSABLE_ENTITY)
            .body(String::from("Sum меньше zero"))
    }
}

#[post("/")]
async fn hello_post() -> HttpResponse {
    info!("Endpoint from mod post");
    HttpResponse::Ok().body("Hello world! POST")
}

#[get("/info/{id}/")]
async fn get_info_by_id(
    other_info: web::Json<Info>,
    path: web::Path<(String,)>,
) -> HttpResponse {
    HttpResponse::Ok().body(format!("User id: {}; User name: {}", path.into_inner().0, other_info.username))
}

#[post("/info/")]
async fn info(info: web::Json<Info>) -> HttpResponse {
    HttpResponse::Ok().body(format!("{}", info.username))
}

pub fn test_scope(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/test")
        .service(hello_get)
        .service(info)
        .service(hello_post)
        .service(get_info_by_id)
        .service(sum_post)
    );
}